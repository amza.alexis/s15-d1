//Mini Activity
//1
let favefood = "Mango Float";
console.log(favefood);
//2
let sum;
sum = 150 + 9;
console.log (sum)
//3
let product;
product = 100 * 90;
console.log(product)
//4
let isActive = true;
console.log(isActive);
//5
let restaurant = ["Da Won","Jollibee","Angel's Pizza","Enye","Boosog"];
console.log(restaurant);
//6
let favesinger = {
    firstName: "Taylor",
    lastName: "Swift",
    stageName: "Taylos Swift",
    birthDay: "idk",
    age: "idk",
    bestAlbum: "Fearless",
    bestSong: "Lover",
    isActive: true
}
console.log(favesinger);

//function
function divideNum(num1, num2) {
    return num1/num2;
};

let quotient = divideNum(50,10);
console.log(`The result of the division is: ${quotient}`)

//Discussion
// Mathematical operators
let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

// num1 = num1 +  num4;
num1 += num4;
console.log(num1);

// num2 = num2 + num4;
num2 += num4;
console.log(num2);

// num1 = num1 * 2;
num1 *= 2;
console.log(num1);

let string1 = "Boston ";
let string2 = " Celtics";

// string1 = string1 + string2;
string1 += string2;
console.log(string1);

// num1 = num1 - string1;
num1 -= string1;
console.log(num1);

let string3 = "Hello everyone";
let myArray = string3.split("", 3);
console.log(myArray);

// Mathematical operations - follows MDAS.

let mdasResult = 1 + 2 - 3 * 4 / 5;
/*
	3*4 = 12
	12/5 = 2.4
	1+2 =3
	3-2.4 = 0.6

 */
console.log(mdasResult);
// PEMDAS - Parenthesis, exponents, multiplication, division, addition and subtraction
let pemdasResult = 1 + (2-3) * (4/5);
/*
	4/5 = .8
	2-3 = -1
	-1*.8 = -0.8
	1+ -0.8 = .0
*/
console.log(pemdasResult);

// Increment and Decrement
// Two types Increment: Pre-fix and Post-fix

let z = 1;
//Pre-fix Incrementation
++z;
console.log(z);

// Post-fix Incrementation
z++;
console.log(z);
console.log(z++);
console.log(z);

// Pre-fix vs Post-fix Incrementation
console.log(z++);
console.log(z);

console.log(++z);

let n = 1;
console.log(++n);// 1 + n = 2
console.log(n);

console.log(n++);// n + 1 = 3
console.log(n);


// Pre-fix and Post-fix Decrementation
console.log(z);
console.log(z--);
console.log(z);

// comparison Operators - used to compare values
// Equality or Loose Equality Operator (==)
console.log(1 == 1);//true
console.log('1' == 1);

// strict equality
console.log(1 === 1);
console.log('1' === 1);

console.log('apple' == 'apple');
let isSame = 55 == 55;
console.log(isSame);

console.log(0 == false); // force coercion
console.log(1 == true);
console.log(true == 'true'); //1 != NaN

console.log(true == '1');
console.log('0' == false);

// Strict equality - checks both value and type
console.log(1 === '1');
console.log('Juan' === 'Juan')
console.log('Maria' === 'maria')

// Inequality Operators (!=)
	// Checks whether the operands are NOT equal and/or have different value
	// will do type coercion if the operands have different types:

	console.log('1' != 1);//false
	//false > both operands are converted to numbers
	//'1' converted into number is 1
	//1 converted into number is 1
	//1 == 1
	//not inequal
	
	console.log('James' != 'John')

	console.log(1 != "true");//true
	//with type conversion: true was converted to 1
	//"true" was convered into a number but results NaN
	//1 is not equal to NaN
	//it IS inequal

// strict inequality operator (!==) > it checks whether the two operand have different values and will check if they have different types

	console.log('5' !== 5)//true
	console.log(5 !== 5)//false

let name1 = 'Juan';
let name2 = 'Maria';
let name3 = 'Pedro';
let name4 = 'Perla';

let number1 = 50;
let number2 = 60;
let numString1 = "50";
let numString2 = "60";

console.log(numString1 == number1);
console.log(numString1 === number1);
console.log(numString1 != number1);
console.log(name4 !== name3);
console.log(name1 == 'juan');
console.log(name1 === "Juan");

// Relational Comparison Operators
	// A comparison operator - check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString3 = "5500";

// Greater Than (>)
console.log( x > y);// false
console.log( w > y);

// Less Than (<)
console.log(w < y); //false
console.log(y >= y);//false
console.log(x < 1000);
console.log(numString3 < 1000);//false 
console.log(6000 < 'juan');//false

// Logical Operators
	// And Operator(&&) - both operands on the left and right or all operands added must be true or other it false
	// T && T = T
	// T && F = F
	// F && T = F
	// F && F = F

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1);//false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);//true

let authorization3 = isAdmin && isLegalAge;
console.log(authorization3);//false

let requiredLevel = 95;
let requiredAge = 18;

let authorization4 = isRegistered && requiredLevel === 25;
console.log(authorization4);//false
let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization5);

let userName1 = 'gamer';
let userName2 = 'shadowMaster';
let userAge1 = 15;
let userAge2 = 30;

let registration1 = userName1.length > 9 && userAge1 >= requiredAge;
//.length is a property of strings which determine the number of characters in the string
console.log(registration1);//false

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);

let registration3 = userName1.length > 8 && userAge2 >=  requiredAge;
console.log(registration3);


// OR Operator (|| - double pipe)
/*
	- or operator returns true if at least one of the operands are true
	T || T = T
	T || F = T
	F || T = T
	F || F = F
*/

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge1 >= requiredAge;
console.log(guildRequirement);//false

guildRequirement = isRegistered || userLevel >= requiredLevel || userAge1 >= requiredAge;
console.log(guildRequirement);


let guildRequirement2 = userLevel >= requiredLevel || userAge1 >= requiredAge;
console.log(guildRequirement2)

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin)//false

//Not operator (!)
// it turns a boolean into the opposite value: T = F F = T

let guildAdmin1 = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin1);//true

let opposite1 = !isAdmin;
let opposite2 = !isLegalAge;

console.log(opposite1);//true - isAdmin original value = false
console.log(opposite2);//false - isLegalAge original value = true

// if - if statement will run a code block if the condition specified is true or results to true.
const candy = 100;
if (candy >= 100){
	console.log('You got a cavity!')
}

/*
	if(true){
		block of code
	};
*/

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 30;

if(userName3.length > 10){
	console.log("Welcome to the Game online!")
};

// else statement will be run if the condition given is false of results to false
if(userName3 >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining Noobies Guild");
} else {
	console.log("You too strong to be noob. :( ");
};

// else if - else if executes a statement, if the previous or the original condition is false or resulted to false but another specified condition resulted to true.
if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the noobies guild.");
} else if(userLevel3 > 25){
	console.log("You too strong to be noob.");
} else if(userAge3 < requiredAge){
	console.log("You're too young to join the guild.");
} else {
	console.log("End of the condition,")
}

// if-else in function
function addNum(num1, num2){
	if(typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if both arguments passed are number types.");
		console.log(num1 + num2);
	} else {
		console.log("One or both of the argumetns are not numbers.")
	};
};

addNum(5, 2)

// let customerName = prompt("Enter your name:");
// if(customerName != null) {
// 	document.getElementById("username").value = customerName;
// }

		/*
			Nested if-else if
			Mini-Activity
				add another condition to our nested if statement:
					- check if the password is at least 8 characters long
				add an else statement which will run if both conditions we not met:
					- show an alert which says "Credentials too short."

			Stretch Goals:

			add an else if statement that if the username is less than 8 characters
				- show an alert message "username is too short."
			add an else if statement that if the password is less than 8 characters
				- show an alert messge "password too short"
			
			Push it in Gitlab and paste the URL in our Boodle account s15

		 */

            function login(username, password){
                if(typeof username === "string" && typeof password === "string"){
                    console.log("Both arguments are string.");
                    
                    if (password.length >= 8) {
                        console.log("Welcome")
                    }
                }
            
                else
                {
                    alert("Password is too short")
                }
            
                if(username.length <= 8 && password.length <= 8)
                {
                    alert("Credentials are too short.")
                }
            
                if(username.length <= 8)
                {
                    alert("Username is too short.")
                }
            
                if(password.length <= 8 )
                {
                    alert("Password is too short.");
                }
            }
login("Amzel", "qwerty")

